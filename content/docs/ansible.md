---
title: "Ansible"
weight: 1
# bookFlatSection: false
# bookToc: 6
# bookHidden: false
---
## Prepare Ansible

```bash
virtualenv venv27
source venv27/bin/activate
```

## Test locally

```bash
vagrant init
vagrant up
ansible-playbook -i hosts site.yml
```

## Other examples

Cache facts from all hosts:
```bash
ansible all -i hosts -m setup
```

Run on all eligible hosts `loadbalancers` playbook:
```bash
ansible-playbook -i hosts loadbalancers.yml
```

Run only specifically tagged Ansible role:
```bash
ansible-playbook -i hosts --limit loadbalancers site.yml --tags node_exporter
```

Check all loadbalancers hosts uptime:

```bash
ansible loadbalancers -i hosts -m command -a 'uptime'
```

Run all eligible playbooks only on `loadbalancers` hosts:
```bash
ansible-playbook -i hosts --limit loadbalancers  site.yml
```