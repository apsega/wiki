---
title: "SSH"
weight: 1
# bookFlatSection: false
# bookToc: 6
# bookHidden: false
---
## Configuration

The following configuration helps to fight with ssh timouts to server:

```bash
Host *
  User $USER
  ServerAliveInterval 60
  ServerAliveCountMax 30
  ForwardAgent yes
```

## Key generation

```bash
    ssh-keygen -t rsa -C "email@addres.com" -b 4096
```

Change password for key:

```bash
    ssh-keygen -p <keyname>
```

## Tunneling

Tunnel port through SSH:

```bash
ssh -L 127.0.0.1:5900:127.0.0.1:5900 pi@xxx.xx.xx.xx
```

## Remote Host Verification Has Changed

When connecting to host via ssh receiving this message:

```
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@       WARNING: POSSIBLE DNS SPOOFING DETECTED!          @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
The RSA host key for <HOST> has changed,
and the key for the corresponding IP address <IP>
is unchanged. This could either mean that
DNS SPOOFING is happening or the IP address for the host
and its host key have changed at the same time.
Offending key for IP in /Users/edgaras/.ssh/known_hosts:225
```

Delete SSH entry:

```bash
sed -i -e 225d /Users/edgaras/.ssh/known_hosts
```
