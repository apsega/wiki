---
title: "sed"
weight: 1
# bookFlatSection: false
# bookToc: 6
# bookHidden: false
---
Replace string in a file:

```sh
sed -i -e 's/String to replace/String to be replaces/g' /etc/file/filename.conf
```

Replace string in a file if it contains `/`:

```sh
sed -i -e 's@String/to/replace@String to be replaces@g' /etc/file/filename.conf
```


Replace certain string on 18th line in a file:

```sh
sed -i -e '18s/String to replace/String to be replaces/g' /etc/file/filename.conf
```

Delete certain line number in file:

```sh
sed -i -e 225d /Users/edgaras/.ssh/known_hosts
```
