---
title: "awk"
weight: 1
# bookFlatSection: false
# bookToc: 6
# bookHidden: false
---

## Variables

AWK is automatically going to set a bunch of useful variables for you:

* `NF` – how many fields in the current line
* `NR` – what the current line number is
* `$1`, `$2`, `$3` .. `$9` .. – the value of each field on the current line
* `$0` – the content of the current line
* `RS` - Specifies the record separator
* `FS` - Specifies the field separator
* `OFS` - Specifies the Output separator
* `ORS` - Specifies the Output separator

```sh
// open each file, assign content to "lines"
var NR = 0;
lines.forEach(function (line) {
  NR = NR + 1;
  var fields = line.trim().split(/\s+/);
  var NF = fields.length;
  // the code you write goes here
});
// close all the files
```

