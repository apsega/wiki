---
title: "Prometheus"
weight: 1
# bookFlatSection: false
# bookToc: 6
# bookHidden: false
---
### deleting metrics

```bash
curl -XDELETE -g 'http://localhost:9090/api/v1/series?match[]={job="script-exporter",script=~"ea-script"}'
```

### graceful shutdown

```bash
docker kill --signal="SIGTERM" prometheus
```
