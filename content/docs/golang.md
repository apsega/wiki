---
title: "Golang"
weight: 1
# bookFlatSection: false
# bookToc: 6
# bookHidden: false
---

## Theory

{{< tabs "golang-pics" >}}
{{< tab "Heap and Stacks" >}} ![stacks](/golang/stacks.png) {{< /tab >}}
{{< tab "Addresses and Pointers" >}} ![pointers](/golang/pointers.png) {{< /tab >}}
{{< /tabs >}}